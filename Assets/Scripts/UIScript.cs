﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class UIScript : MonoBehaviour{

    public void loadScene(string sceneName) {
        SceneManager.LoadScene(sceneName);
    }
}
