﻿using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour{

    [Header("Player")]
    public PlayerData playerData;
    public LevelData levelData;
    public bool isControllable = true;
    public int score = 0;

    [Header("Jump")]
    public int jumpCount = 0;
    public bool isJumping = false;

    [Header("UI")]
    public GameObject winningUI;
    public Text pointsText;

    



    private Rigidbody rigidBody;
    private Vector3 spawnPosition;

    #region lifecycle
    void Start(){
        rigidBody = GetComponent<Rigidbody>();
        GetComponent<MeshRenderer>().material.color = playerData.color;

        Instantiate(levelData.levelPrefab);

        spawnPosition = transform.position;
        addPointsAndUpdateUI(0);
    }

    private void Update() {
        if (rigidBody.velocity.y <= 0) {
            isJumping = false;
        } else {
            isJumping = true;
        }
    }

    void FixedUpdate(){
        if (isControllable) { 
            if (Input.GetKey(KeyCode.D)) {
                rigidBody.AddForce(Vector3.right * playerData.speed);
            }

            if (Input.GetKey(KeyCode.A)) {
                rigidBody.AddForce(Vector3.left * playerData.speed);
            }

            if (jumpCount < playerData.maxJumpCount) {
                if (!isJumping) { 
                    if (Input.GetKeyDown(KeyCode.W)) {
                        rigidBody.AddForce(Vector3.up * playerData.jumpForce, ForceMode.Impulse);
                        jumpCount++;
                    }
                }
            }
        }
    }

    private void OnCollisionEnter(Collision collision) {
        switch (collision.gameObject.tag) {
            case "Obstacle":
               jumpCount = 0;
                isJumping = false;
                break;
        }
    }

    private void OnTriggerEnter(Collider triggerCollider) {
        switch (triggerCollider.tag) {
            case "Finish":
                if (score >= levelData.requiredScore) { 
                    isControllable = false;
                    rigidBody.velocity = Vector3.zero;
                    winningUI.SetActive(true);
                }
                break;
            case "Collectable":
                Destroy(triggerCollider.gameObject);
                addPointsAndUpdateUI(1);
                break;
            case "Respawn":
                Respawn();
                break;
        }
    }
    #endregion


    #region helper
    public void Respawn() {
        rigidBody.velocity = Vector3.zero;
        transform.position = spawnPosition;
    }

    private void addPointsAndUpdateUI(int additionalPoints) {
        score += additionalPoints;
        pointsText.text = score.ToString() + " / " + levelData.requiredScore.ToString();
    }
    #endregion
}
