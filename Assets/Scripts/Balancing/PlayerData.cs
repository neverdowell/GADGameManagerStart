﻿using UnityEngine;

[CreateAssetMenu(menuName = "GameManager/Player Data")]
public class PlayerData : ScriptableObject {
    public float speed = 0;
    public float jumpForce = 0;
    public int maxJumpCount = 1;
    public Color color;
}
