﻿using UnityEngine;

[CreateAssetMenu(menuName = "GameManager/Level Data")]
public class LevelData : ScriptableObject {

    public GameObject levelPrefab;
    public int requiredScore = 0;
}
