﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject target;
    public Vector3 offset;

    #region lifecycle
    void Start(){
        offset = transform.position - target.transform.position;
    }

    void Update(){
        transform.position = target.transform.position + offset;
        transform.LookAt(target.transform);
    }
    #endregion
}
